function NotFoundScreen(): JSX.Element {
  return(
    <div className="not-found-screen">
      <div className="not-found-screen">404 Not Found</div>
      <a href="/">Main page</a>
    </div>
  );
}

export default NotFoundScreen;
