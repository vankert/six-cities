import Header from "../../components/header/header";
import CitiesCard from "../../components/cities-card/cities-card";
import NotFoundScreen from "../404-screen/404-screen";
import ReviewsForm from "../../components/reviews-form/reviews-form";
import { Room } from "../../types/room";
import { Comment } from "../../types/comment";
import {useParams} from 'react-router-dom';
import { useState } from "react";

import {Helmet} from 'react-helmet-async';
import dayjs from 'dayjs';

import { comments } from "../../mock/comments";

type MainScreenProps = {
  rooms: Room[];
}

function RoomScreen({rooms}: MainScreenProps): JSX.Element {
  const params = useParams();
  const room: Room | undefined = rooms.find((room) => room.id.toString() === params.id);
  const [isFavorite, setIsFavorite] = useState(room?.isFavorite);

  const currentComments: Comment[] | undefined = comments.filter((comment) => comment.id.toString() === params.id);

  const roomsList = rooms.slice(0, 3);

  const ratingStyle = room ? Math.floor(room.rating) * 20 : null;

  const getCommentRating = (rating : number): number => {
    return Math.floor(rating) * 20;
  }

  let key = 1;

  return room ? (
      <div className="page">
        <Helmet>
          <title>Room | {room.title}</title>
        </Helmet>

      <Header/>

      <main className="page__main page__main--property">
        <section className="property">
          <div className="property__gallery-container container">
            <div className="property__gallery">
              {
                room.images.map((img)=>{

                  return (
                    <div key={key++} className="property__image-wrapper">
                      <img className="property__image" src={img} alt="Photo studio"/>
                    </div>
                  )
                })
              }
            </div>
          </div>
          <div className="property__container container">
            <div className="property__wrapper">
            {room.isPremium ? <div className="property__mark"><span>Premium</span></div> : null}

              <div className="property__name-wrapper">
                <h1 className="property__name">
                  {room.title}
                </h1>
                <button
                  onClick={()=>setIsFavorite(!isFavorite)}
                  className={`property__bookmark-button button ${isFavorite ? 'property__bookmark-button--active' : ''}`}
                  type="button"
                  >
                  <svg className="property__bookmark-icon" width="31" height="33">
                    <use xlinkHref="#icon-bookmark"></use>
                  </svg>
                  <span className="visually-hidden">To bookmarks</span>
                </button>
              </div>
              <div className="property__rating rating">
                <div className="property__stars rating__stars">
                  <span style={{width: `${String(ratingStyle)}%`}}></span>
                  <span className="visually-hidden">Rating</span>
                </div>
                <span className="property__rating-value rating__value">{room.rating}</span>
              </div>
              <ul className="property__features">
                <li className="property__feature property__feature--entire">
                  {room.type}
                </li>
                <li className="property__feature property__feature--bedrooms">
                  {room.bedrooms} Bedrooms
                </li>
                <li className="property__feature property__feature--adults">
                  Max {room.maxAdults} adults
                </li>
              </ul>
              <div className="property__price">
                <b className="property__price-value">&euro;{room.price}</b>
                <span className="property__price-text">&nbsp;night</span>
              </div>
              <div className="property__inside">
                <h2 className="property__inside-title">What&apos;s inside</h2>
                <ul className="property__inside-list">
                  {
                    room.goods.map((good)=>(
                      <li key={key++} className="property__inside-item">
                        {good}
                      </li>
                    ))
                  }
                </ul>
              </div>
              <div className="property__host">
                <h2 className="property__host-title">Meet the host</h2>
                <div className="property__host-user user">
                  <div className={`property__avatar-wrapper user__avatar-wrapper ${room.host.isPro ? 'property__avatar-wrapper--pro ' : ''}`}>
                    <img className="property__avatar user__avatar" src={room.host.avatarUrl} width="74" height="74" alt="Host avatar"/>
                  </div>
                  <span className="property__user-name">
                    {room.host.name}
                  </span>
                  {room.host.isPro ? <span className="property__user-status">Pro</span> : null}

                </div>
                <div className="property__description">
                  <p className="property__text">
                    {room.description}
                  </p>
                </div>
              </div>
              <section className="property__reviews reviews">
                <h2 className="reviews__title">Reviews &middot; <span className="reviews__amount">{currentComments.length}</span></h2>
                <ul className="reviews__list">
                  {
                    currentComments.map((comment) => {
                      const date = comment.date;
                      const formattedDate = dayjs(date).format('MMMM YYYY');

                      return (
                        <li key={key++} className="reviews__item">
                          <div className="reviews__user user">
                            <div className="reviews__avatar-wrapper user__avatar-wrapper">
                              <img className="reviews__avatar user__avatar" src={comment.user.avatarUrl} width="54" height="54" alt="Reviews avatar"/>
                            </div>
                            <span className="reviews__user-name">
                              {comment.user.name}
                            </span>
                          </div>
                          <div className="reviews__info">
                            <div className="reviews__rating rating">
                              <div className="reviews__stars rating__stars">
                                <span style={{width: `${getCommentRating(comment.rating)}%`}}></span>
                                <span className="visually-hidden">Rating</span>
                              </div>
                            </div>
                            <p className="reviews__text">
                              {comment.comment}
                            </p>
                            <time className="reviews__time" dateTime={comment.date}>{formattedDate}</time>
                          </div>
                        </li>
                      )
                    })
                  }

                </ul>
                <ReviewsForm/>
              </section>
            </div>
          </div>
          <section className="property__map map"></section>
        </section>

        <div className="container">
          <section className="near-places places">
            <h2 className="near-places__title">Other places in the neighbourhood</h2>
            <div className="near-places__list places__list">
              {
                  roomsList.map((room)=> {
                    return (
                      <CitiesCard key={room.id} room={room}/>
                    );
                  })
                }
            </div>
          </section>
        </div>
      </main>
    </div>
    ) : (
      <NotFoundScreen/>
    );

}

export default RoomScreen;
