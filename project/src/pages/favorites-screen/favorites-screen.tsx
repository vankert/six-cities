import Header from "../../components/header/header";
import Footer from "../../components/footer/footer";
import CitiesCard from "../../components/cities-card/cities-card";

import { Room } from "../../types/room";

import {Helmet} from 'react-helmet-async';

type FavoritesScreenProps = {
  rooms: Room[];
}

function FavoritesScreen({rooms}: FavoritesScreenProps): JSX.Element {
  const roomsList = rooms;

  return (
    <div className="page">
      <Helmet>
        <title>Your favorites</title>
      </Helmet>

      <Header/>

      <main className="page__main page__main--favorites">
        <div className="page__favorites-container container">
          <section className="favorites">
            <h1 className="favorites__title">Saved listing</h1>
            <ul className="favorites__list">
              <li className="favorites__locations-items">
                <div className="favorites__locations locations locations--current">
                  <div className="locations__item">
                    <a className="locations__item-link" href="#">
                      <span>Amsterdam</span>
                    </a>
                  </div>
                </div>
                <div className="favorites__places">
                  {
                    roomsList.map((room)=> {
                      return (
                        <CitiesCard key={room.id} room={room}/>
                      );
                    })
                  }
                </div>
              </li>

              <li className="favorites__locations-items">
                <div className="favorites__locations locations locations--current">
                  <div className="locations__item">
                    <a className="locations__item-link" href="#">
                      <span>Cologne</span>
                    </a>
                  </div>
                </div>
                <div className="favorites__places">
                  {
                    roomsList.map((room)=> {
                      return (
                        <CitiesCard key={room.id} room={room}/>
                      );
                    })
                  }
                </div>
              </li>
            </ul>
          </section>
        </div>
      </main>

      <Footer/>
    </div>
  );
}

export default FavoritesScreen;
