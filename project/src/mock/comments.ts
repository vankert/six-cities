const comments = [
  {
    "comment": "A quiet cozy and picturesque that hides behind a a river by the unique lightness of Amsterdam!!!!",
    "date": "2022-05-04T08:33:25.726Z",
    "id": 1,
    "rating": 3,
    "user": {
      "avatarUrl": "img/avatar-max.jpg",
      "id": 1,
      "isPro": false,
      "name": "Oliver"
    }
  },
  {
    "comment": "A quiet cozy and picturesque that hides behind a a river by the unique lightness of Amsterdam.",
    "date": "2023-01-04T08:33:25.726Z",
    "id": 1,
    "rating": 4,
    "user": {
      "avatarUrl": "img/avatar-max.jpg",
      "id": 1,
      "isPro": false,
      "name": "Oliver"
    }
  },
  {
    "comment": "A quiet cozy and picturesque that hides behind a a river by the unique lightness of Amsterdam.",
    "date": "2023-11-04T08:33:25.726Z",
    "id": 1,
    "rating": 5,
    "user": {
      "avatarUrl": "img/avatar-max.jpg",
      "id": 1,
      "isPro": false,
      "name": "Oliver"
    }
  },
  {
    "comment": "A quiet cozy and picturesque that hides behind a a river by the unique lightness of Amsterdam.",
    "date": "2023-09-04T08:33:25.726Z",
    "id": 1,
    "rating": 2,
    "user": {
      "avatarUrl": "img/avatar-max.jpg",
      "id": 1,
      "isPro": false,
      "name": "Oliver"
    }
  },
  {
    "comment": "A quiet cozy and picturesque that hides behind a a river by the unique lightness of Amsterdam.",
    "date": "2023-09-04T08:33:25.726Z",
    "id": 1,
    "rating": 1,
    "user": {
      "avatarUrl": "img/avatar-max.jpg",
      "id": 1,
      "isPro": false,
      "name": "Oliver"
    }
  }
];

export {comments};
