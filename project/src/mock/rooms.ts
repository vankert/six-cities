const rooms = [
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/19.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "hotel",
    "bedrooms": 3,
    "maxAdults": 7,
    "price": 303,
    "goods": [
      "Washer",
      "Baby seat",
      "Air conditioning",
      "Fridge",
      "Laptop friendly workspace",
      "Breakfast",
      "Towels"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 52.37454,
      "longitude": 4.881976,
      "zoom": 16
    },
    "id": 1
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "The Pondhouse - A Magical Place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 8,
    "price": 134,
    "goods": [
      "Washer",
      "Dishwasher",
      "Air conditioning",
      "Baby seat",
      "Towels",
      "Laptop friendly workspace",
      "Fridge",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 51.235402,
      "longitude": 6.800314,
      "zoom": 16
    },
    "id": 2
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/7.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "Waterfront with extraordinary view",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.4,
    "type": "house",
    "bedrooms": 1,
    "maxAdults": 4,
    "price": 173,
    "goods": [
      "Baby seat",
      "Washer",
      "Air conditioning",
      "Laptop friendly workspace",
      "Towels",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 52.361540000000005,
      "longitude": 4.883976,
      "zoom": 16
    },
    "id": 3
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/14.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.6,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 212,
    "goods": [
      "Air conditioning",
      "Fridge",
      "Dishwasher",
      "Washing machine",
      "Baby seat",
      "Towels",
      "Breakfast",
      "Laptop friendly workspace",
      "Washer",
      "Coffee machine"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 51.239402000000005,
      "longitude": 6.756314000000001,
      "zoom": 16
    },
    "id": 4
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.4,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 4,
    "price": 152,
    "goods": [
      "Breakfast",
      "Air conditioning",
      "Washer",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 50.932361,
      "longitude": 6.960974,
      "zoom": 16
    },
    "id": 5
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/13.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.7,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 6,
    "price": 464,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast",
      "Air conditioning",
      "Towels",
      "Washer",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 53.555341000000006,
      "longitude": 9.975654,
      "zoom": 16
    },
    "id": 6
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/10.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg"
    ],
    "title": "Nice, cozy, warm big bed apartment",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.3,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 3,
    "price": 138,
    "goods": [
      "Laptop friendly workspace",
      "Baby seat",
      "Washer",
      "Towels",
      "Breakfast",
      "Fridge",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 51.211402,
      "longitude": 6.756314000000001,
      "zoom": 16
    },
    "id": 7
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/1.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg"
    ],
    "title": "Canal View Prinsengracht",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.3,
    "type": "apartment",
    "bedrooms": 3,
    "maxAdults": 8,
    "price": 279,
    "goods": [
      "Air conditioning",
      "Breakfast",
      "Baby seat",
      "Washer",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 52.36854,
      "longitude": 4.887976,
      "zoom": 16
    },
    "id": 8
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/6.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg"
    ],
    "title": "Waterfront with extraordinary view",
    "isFavorite": false,
    "isPremium": true,
    "rating": 3.9,
    "type": "hotel",
    "bedrooms": 5,
    "maxAdults": 7,
    "price": 248,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast",
      "Washer",
      "Baby seat",
      "Air conditioning",
      "Towels"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 53.534341000000005,
      "longitude": 9.998654,
      "zoom": 16
    },
    "id": 9
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/14.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.4,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 8,
    "price": 504,
    "goods": [
      "Laptop friendly workspace",
      "Air conditioning",
      "Baby seat",
      "Breakfast",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 53.573341000000006,
      "longitude": 10.009654000000001,
      "zoom": 16
    },
    "id": 10
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/15.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.5,
    "type": "apartment",
    "bedrooms": 3,
    "maxAdults": 9,
    "price": 299,
    "goods": [
      "Air conditioning",
      "Baby seat",
      "Breakfast",
      "Washer",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 48.877610000000004,
      "longitude": 2.333499,
      "zoom": 16
    },
    "id": 11
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/12.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg"
    ],
    "title": "Nice, cozy, warm big bed apartment",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.1,
    "type": "hotel",
    "bedrooms": 2,
    "maxAdults": 4,
    "price": 308,
    "goods": [
      "Laptop friendly workspace",
      "Fridge",
      "Breakfast",
      "Baby seat",
      "Air conditioning",
      "Dishwasher",
      "Towels",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 48.865610000000004,
      "longitude": 2.350499,
      "zoom": 16
    },
    "id": 12
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/12.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.7,
    "type": "apartment",
    "bedrooms": 3,
    "maxAdults": 8,
    "price": 377,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 51.250402,
      "longitude": 6.7853140000000005,
      "zoom": 16
    },
    "id": 13
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/4.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": true,
    "rating": 3.8,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 6,
    "price": 662,
    "goods": [
      "Washing machine",
      "Towels",
      "Washer",
      "Coffee machine",
      "Air conditioning",
      "Dishwasher",
      "Baby seat",
      "Laptop friendly workspace",
      "Breakfast",
      "Fridge",
      "Cable TV"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 48.87861,
      "longitude": 2.357499,
      "zoom": 16
    },
    "id": 14
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/13.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 6,
    "price": 394,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace",
      "Washer",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.865556999999995,
      "longitude": 4.371696999999999,
      "zoom": 16
    },
    "id": 15
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/14.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.6,
    "type": "apartment",
    "bedrooms": 1,
    "maxAdults": 10,
    "price": 355,
    "goods": [
      "Baby seat",
      "Air conditioning",
      "Laptop friendly workspace",
      "Washer",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.867557,
      "longitude": 4.371696999999999,
      "zoom": 16
    },
    "id": 16
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/20.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Beautiful & luxurious apartment at great location",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 3,
    "price": 902,
    "goods": [
      "Laptop friendly workspace",
      "Dishwasher",
      "Washing machine",
      "Washer",
      "Fridge",
      "Towels",
      "Breakfast",
      "Baby seat",
      "Coffee machine",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 50.957361,
      "longitude": 6.9509739999999995,
      "zoom": 16
    },
    "id": 17
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/17.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg"
    ],
    "title": "Beautiful & luxurious apartment at great location",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.6,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 189,
    "goods": [
      "Air conditioning",
      "Laptop friendly workspace",
      "Washer",
      "Breakfast",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 48.862610000000004,
      "longitude": 2.369499,
      "zoom": 16
    },
    "id": 18
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/2.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.7,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 242,
    "goods": [
      "Washer",
      "Baby seat",
      "Breakfast",
      "Towels",
      "Air conditioning",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 50.930361,
      "longitude": 6.937974,
      "zoom": 16
    },
    "id": 19
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/17.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg"
    ],
    "title": "Canal View Prinsengracht",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "hotel",
    "bedrooms": 4,
    "maxAdults": 9,
    "price": 216,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast",
      "Washer",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 50.945361,
      "longitude": 6.935974,
      "zoom": 16
    },
    "id": 20
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/6.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.1,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 7,
    "price": 516,
    "goods": [
      "Breakfast",
      "Air conditioning",
      "Towels",
      "Baby seat",
      "Washer",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 48.87561,
      "longitude": 2.375499,
      "zoom": 16
    },
    "id": 21
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/4.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.1,
    "type": "house",
    "bedrooms": 3,
    "maxAdults": 5,
    "price": 705,
    "goods": [
      "Laptop friendly workspace",
      "Baby seat",
      "Breakfast",
      "Washer",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 52.364540000000005,
      "longitude": 4.9019759999999994,
      "zoom": 16
    },
    "id": 22
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/8.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg"
    ],
    "title": "Waterfront with extraordinary view",
    "isFavorite": false,
    "isPremium": true,
    "rating": 4.3,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 8,
    "price": 289,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 50.913361,
      "longitude": 6.9509739999999995,
      "zoom": 16
    },
    "id": 23
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/18.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg"
    ],
    "title": "Tile House",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.7,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 237,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 51.237402,
      "longitude": 6.779314,
      "zoom": 16
    },
    "id": 24
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/19.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "apartment",
    "bedrooms": 5,
    "maxAdults": 9,
    "price": 207,
    "goods": [
      "Towels",
      "Laptop friendly workspace",
      "Washer",
      "Breakfast",
      "Fridge",
      "Air conditioning",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 52.37854,
      "longitude": 4.894976,
      "zoom": 16
    },
    "id": 25
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 197,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Fridge",
      "Baby seat",
      "Coffee machine",
      "Air conditioning",
      "Washing machine",
      "Towels",
      "Breakfast",
      "Dishwasher"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 48.843610000000005,
      "longitude": 2.338499,
      "zoom": 16
    },
    "id": 26
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg"
    ],
    "title": "The Joshua Tree House",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2,
    "type": "apartment",
    "bedrooms": 4,
    "maxAdults": 4,
    "price": 153,
    "goods": [
      "Breakfast",
      "Washer",
      "Towels",
      "Baby seat",
      "Laptop friendly workspace",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.204402,
      "longitude": 6.7773140000000005,
      "zoom": 16
    },
    "id": 27
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/17.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.9,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 253,
    "goods": [
      "Towels",
      "Dishwasher",
      "Air conditioning",
      "Breakfast",
      "Baby seat",
      "Laptop friendly workspace",
      "Fridge",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 53.574341000000004,
      "longitude": 10.022654000000001,
      "zoom": 16
    },
    "id": 28
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/15.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Tile House",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.4,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 7,
    "price": 657,
    "goods": [
      "Baby seat",
      "Air conditioning",
      "Breakfast",
      "Fridge",
      "Cable TV",
      "Dishwasher",
      "Laptop friendly workspace",
      "Washing machine",
      "Towels",
      "Coffee machine",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 52.36354,
      "longitude": 4.911976,
      "zoom": 16
    },
    "id": 29
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/3.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Beautiful & luxurious apartment at great location",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.3,
    "type": "apartment",
    "bedrooms": 2,
    "maxAdults": 8,
    "price": 389,
    "goods": [
      "Towels",
      "Baby seat",
      "Coffee machine",
      "Washer",
      "Fridge",
      "Laptop friendly workspace",
      "Breakfast",
      "Air conditioning",
      "Washing machine",
      "Dishwasher"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Design interior in most sympathetic area! Complitely renovated, well-equipped, cosy studio in idyllic, over 100 years old wooden house. Calm street, fast connection to center and airport.",
    "location": {
      "latitude": 53.540341000000005,
      "longitude": 10.025654000000001,
      "zoom": 16
    },
    "id": 30
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/13.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "Canal View Prinsengracht",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.2,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 5,
    "price": 437,
    "goods": [
      "Air conditioning",
      "Laptop friendly workspace",
      "Breakfast",
      "Washer",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 50.954361,
      "longitude": 6.982974,
      "zoom": 16
    },
    "id": 31
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/10.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Tile House",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.4,
    "type": "hotel",
    "bedrooms": 4,
    "maxAdults": 4,
    "price": 254,
    "goods": [
      "Breakfast",
      "Baby seat",
      "Washer",
      "Towels",
      "Air conditioning",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 50.839557,
      "longitude": 4.346697,
      "zoom": 16
    },
    "id": 32
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2,
    "type": "apartment",
    "bedrooms": 3,
    "maxAdults": 6,
    "price": 127,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 50.934361,
      "longitude": 6.943974,
      "zoom": 16
    },
    "id": 33
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.4,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 139,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast",
      "Washer",
      "Baby seat",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Design interior in most sympathetic area! Complitely renovated, well-equipped, cosy studio in idyllic, over 100 years old wooden house. Calm street, fast connection to center and airport.",
    "location": {
      "latitude": 48.837610000000005,
      "longitude": 2.364499,
      "zoom": 16
    },
    "id": 34
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/18.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.7,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 194,
    "goods": [
      "Laptop friendly workspace",
      "Washer",
      "Towels",
      "Baby seat",
      "Breakfast",
      "Air conditioning",
      "Dishwasher",
      "Fridge"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 52.36954000000001,
      "longitude": 4.914976,
      "zoom": 16
    },
    "id": 35
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/2.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg"
    ],
    "title": "The Joshua Tree House",
    "isFavorite": false,
    "isPremium": true,
    "rating": 4.9,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 6,
    "price": 371,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 53.529341,
      "longitude": 9.975654,
      "zoom": 16
    },
    "id": 36
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/2.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg"
    ],
    "title": "The Pondhouse - A Magical Place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.7,
    "type": "apartment",
    "bedrooms": 2,
    "maxAdults": 6,
    "price": 460,
    "goods": [
      "Dishwasher",
      "Breakfast",
      "Towels",
      "Laptop friendly workspace",
      "Baby seat",
      "Fridge",
      "Washer",
      "Air conditioning",
      "Washing machine",
      "Coffee machine"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.228402,
      "longitude": 6.784314,
      "zoom": 16
    },
    "id": 37
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/8.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.4,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 200,
    "goods": [
      "Fridge",
      "Dishwasher",
      "Laptop friendly workspace",
      "Washer",
      "Breakfast",
      "Towels",
      "Baby seat",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.918461,
      "longitude": 6.969974,
      "zoom": 16
    },
    "id": 38
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/7.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "apartment",
    "bedrooms": 3,
    "maxAdults": 9,
    "price": 155,
    "goods": [
      "Laptop friendly workspace",
      "Fridge",
      "Air conditioning",
      "Baby seat",
      "Towels",
      "Washer",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 52.397540000000006,
      "longitude": 4.9099759999999995,
      "zoom": 16
    },
    "id": 39
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/18.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg"
    ],
    "title": "The Joshua Tree House",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.4,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 3,
    "price": 118,
    "goods": [
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 48.868610000000004,
      "longitude": 2.342499,
      "zoom": 16
    },
    "id": 40
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/10.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.5,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 3,
    "price": 184,
    "goods": [
      "Laptop friendly workspace",
      "Washer",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.860557,
      "longitude": 4.376697,
      "zoom": 16
    },
    "id": 41
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4,
    "type": "house",
    "bedrooms": 5,
    "maxAdults": 8,
    "price": 237,
    "goods": [
      "Breakfast",
      "Baby seat",
      "Laptop friendly workspace",
      "Air conditioning",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 50.854557,
      "longitude": 4.364697,
      "zoom": 16
    },
    "id": 42
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/15.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg"
    ],
    "title": "The Joshua Tree House",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.4,
    "type": "house",
    "bedrooms": 5,
    "maxAdults": 8,
    "price": 535,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 48.834610000000005,
      "longitude": 2.364499,
      "zoom": 16
    },
    "id": 43
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/5.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.9,
    "type": "hotel",
    "bedrooms": 5,
    "maxAdults": 6,
    "price": 184,
    "goods": [
      "Towels",
      "Air conditioning",
      "Baby seat",
      "Fridge",
      "Washer",
      "Dishwasher",
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 52.35054,
      "longitude": 4.908976,
      "zoom": 16
    },
    "id": 44
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/5.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 148,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Baby seat",
      "Breakfast",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 51.205402,
      "longitude": 6.7613140000000005,
      "zoom": 16
    },
    "id": 45
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/12.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg"
    ],
    "title": "The Pondhouse - A Magical Place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.4,
    "type": "house",
    "bedrooms": 3,
    "maxAdults": 5,
    "price": 611,
    "goods": [
      "Baby seat",
      "Towels",
      "Laptop friendly workspace",
      "Washer",
      "Air conditioning",
      "Fridge",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 50.917361,
      "longitude": 6.977974,
      "zoom": 16
    },
    "id": 46
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.1,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 183,
    "goods": [
      "Towels",
      "Air conditioning",
      "Laptop friendly workspace",
      "Washer",
      "Baby seat",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 53.570341000000006,
      "longitude": 9.975654,
      "zoom": 16
    },
    "id": 47
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg"
    ],
    "title": "The Pondhouse - A Magical Place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.3,
    "type": "hotel",
    "bedrooms": 1,
    "maxAdults": 6,
    "price": 420,
    "goods": [
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 50.852557,
      "longitude": 4.3376969999999995,
      "zoom": 16
    },
    "id": 48
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/16.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.1,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 171,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.949361,
      "longitude": 6.976974,
      "zoom": 16
    },
    "id": 49
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg"
    ],
    "title": "House in countryside",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.9,
    "type": "apartment",
    "bedrooms": 2,
    "maxAdults": 10,
    "price": 249,
    "goods": [
      "Washer",
      "Coffee machine",
      "Washing machine",
      "Breakfast",
      "Dishwasher",
      "Towels",
      "Laptop friendly workspace",
      "Air conditioning",
      "Baby seat",
      "Fridge"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.237402,
      "longitude": 6.797314,
      "zoom": 16
    },
    "id": 50
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/2.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.7,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 9,
    "price": 922,
    "goods": [
      "Dishwasher",
      "Washer",
      "Fridge",
      "Baby seat",
      "Breakfast",
      "Laptop friendly workspace",
      "Air conditioning",
      "Coffee machine",
      "Towels"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 50.849557,
      "longitude": 4.374696999999999,
      "zoom": 16
    },
    "id": 51
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.9,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 8,
    "price": 785,
    "goods": [
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 50.842557,
      "longitude": 4.363696999999999,
      "zoom": 16
    },
    "id": 52
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/15.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.6,
    "type": "hotel",
    "bedrooms": 5,
    "maxAdults": 6,
    "price": 221,
    "goods": [
      "Air conditioning",
      "Breakfast",
      "Washer",
      "Baby seat",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 51.216402,
      "longitude": 6.758314,
      "zoom": 16
    },
    "id": 53
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.4,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 9,
    "price": 838,
    "goods": [
      "Air conditioning",
      "Fridge",
      "Laptop friendly workspace",
      "Dishwasher",
      "Towels",
      "Washer",
      "Breakfast",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 52.389540000000004,
      "longitude": 4.883976,
      "zoom": 16
    },
    "id": 54
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/6.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 111,
    "goods": [
      "Laptop friendly workspace",
      "Dishwasher",
      "Towels",
      "Air conditioning",
      "Washer",
      "Fridge",
      "Breakfast",
      "Coffee machine",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 48.858610000000006,
      "longitude": 2.330499,
      "zoom": 16
    },
    "id": 55
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/7.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.5,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 5,
    "price": 159,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 53.565341,
      "longitude": 9.980654000000001,
      "zoom": 16
    },
    "id": 56
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/6.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.2,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 122,
    "goods": [
      "Baby seat",
      "Fridge",
      "Air conditioning",
      "Washer",
      "Breakfast",
      "Laptop friendly workspace",
      "Towels"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Design interior in most sympathetic area! Complitely renovated, well-equipped, cosy studio in idyllic, over 100 years old wooden house. Calm street, fast connection to center and airport.",
    "location": {
      "latitude": 51.232402,
      "longitude": 6.800314,
      "zoom": 16
    },
    "id": 57
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/10.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg"
    ],
    "title": "Tile House",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.5,
    "type": "house",
    "bedrooms": 3,
    "maxAdults": 4,
    "price": 399,
    "goods": [
      "Washer",
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Peaceful studio in the most wanted area in town. Quiet house Near of everything. Completely renovated. Lovely neighbourhood, lot of trendy shops, restaurants and bars in a walking distance.",
    "location": {
      "latitude": 50.837557,
      "longitude": 4.339697,
      "zoom": 16
    },
    "id": 58
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.8,
    "type": "apartment",
    "bedrooms": 2,
    "maxAdults": 3,
    "price": 439,
    "goods": [
      "Fridge",
      "Baby seat",
      "Dishwasher",
      "Air conditioning",
      "Washing machine",
      "Cable TV",
      "Breakfast",
      "Washer",
      "Laptop friendly workspace",
      "Towels",
      "Coffee machine"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 51.214402,
      "longitude": 6.764314000000001,
      "zoom": 16
    },
    "id": 59
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/3.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": true,
    "rating": 4.5,
    "type": "house",
    "bedrooms": 1,
    "maxAdults": 4,
    "price": 928,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace",
      "Washer",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 50.947361,
      "longitude": 6.9799739999999995,
      "zoom": 16
    },
    "id": 60
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/9.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.4,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 9,
    "price": 653,
    "goods": [
      "Air conditioning",
      "Breakfast",
      "Washer",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 48.87961000000001,
      "longitude": 2.353499,
      "zoom": 16
    },
    "id": 61
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/19.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg"
    ],
    "title": "The Joshua Tree House",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.8,
    "type": "hotel",
    "bedrooms": 4,
    "maxAdults": 9,
    "price": 170,
    "goods": [
      "Air conditioning",
      "Baby seat",
      "Washer",
      "Laptop friendly workspace",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.243402,
      "longitude": 6.791314,
      "zoom": 16
    },
    "id": 62
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/10.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "Penthouse, 4-5 rooms + 5 balconies",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.9,
    "type": "hotel",
    "bedrooms": 5,
    "maxAdults": 8,
    "price": 466,
    "goods": [
      "Air conditioning",
      "Coffee machine",
      "Breakfast",
      "Baby seat",
      "Washer",
      "Towels",
      "Fridge",
      "Laptop friendly workspace",
      "Dishwasher"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.248402000000006,
      "longitude": 6.763314,
      "zoom": 16
    },
    "id": 63
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/1.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.1,
    "type": "hotel",
    "bedrooms": 3,
    "maxAdults": 9,
    "price": 270,
    "goods": [
      "Laptop friendly workspace",
      "Washer",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 50.844556999999995,
      "longitude": 4.346697,
      "zoom": 16
    },
    "id": 64
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/19.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.7,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 5,
    "price": 225,
    "goods": [
      "Laptop friendly workspace",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.241402,
      "longitude": 6.782314,
      "zoom": 16
    },
    "id": 65
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/4.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.9,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 177,
    "goods": [
      "Fridge",
      "Air conditioning",
      "Laptop friendly workspace",
      "Towels",
      "Baby seat",
      "Washer",
      "Breakfast",
      "Dishwasher"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.959361,
      "longitude": 6.978974,
      "zoom": 16
    },
    "id": 66
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/5.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.6,
    "type": "hotel",
    "bedrooms": 1,
    "maxAdults": 3,
    "price": 357,
    "goods": [
      "Laptop friendly workspace",
      "Baby seat",
      "Air conditioning",
      "Washing machine",
      "Breakfast",
      "Towels",
      "Coffee machine",
      "Washer",
      "Cable TV",
      "Dishwasher",
      "Fridge"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Design interior in most sympathetic area! Complitely renovated, well-equipped, cosy studio in idyllic, over 100 years old wooden house. Calm street, fast connection to center and airport.",
    "location": {
      "latitude": 50.835556999999994,
      "longitude": 4.354697,
      "zoom": 16
    },
    "id": 67
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/19.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg"
    ],
    "title": "Waterfront with extraordinary view",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.5,
    "type": "hotel",
    "bedrooms": 5,
    "maxAdults": 6,
    "price": 383,
    "goods": [
      "Breakfast",
      "Towels",
      "Laptop friendly workspace",
      "Fridge",
      "Washer",
      "Air conditioning",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 50.827557,
      "longitude": 4.336697,
      "zoom": 16
    },
    "id": 68
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/3.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "The Pondhouse - A Magical Place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3,
    "type": "hotel",
    "bedrooms": 1,
    "maxAdults": 4,
    "price": 383,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 51.210402,
      "longitude": 6.798314,
      "zoom": 16
    },
    "id": 69
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/18.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.2,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 114,
    "goods": [
      "Washer",
      "Baby seat",
      "Laptop friendly workspace",
      "Air conditioning",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 50.862556999999995,
      "longitude": 4.375697,
      "zoom": 16
    },
    "id": 70
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/4.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.1,
    "type": "hotel",
    "bedrooms": 3,
    "maxAdults": 7,
    "price": 109,
    "goods": [
      "Breakfast",
      "Towels",
      "Baby seat",
      "Laptop friendly workspace",
      "Washer",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 51.222402,
      "longitude": 6.786314,
      "zoom": 16
    },
    "id": 71
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/4.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.5,
    "type": "house",
    "bedrooms": 3,
    "maxAdults": 7,
    "price": 681,
    "goods": [
      "Laptop friendly workspace",
      "Towels",
      "Washer",
      "Air conditioning",
      "Baby seat",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 51.225402,
      "longitude": 6.784314,
      "zoom": 16
    },
    "id": 72
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/1.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg"
    ],
    "title": "House in countryside",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.5,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 9,
    "price": 981,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Baby seat",
      "Breakfast",
      "Towels",
      "Air conditioning",
      "Fridge"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 48.84761,
      "longitude": 2.356499,
      "zoom": 16
    },
    "id": 73
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/11.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg"
    ],
    "title": "House in countryside",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.9,
    "type": "house",
    "bedrooms": 2,
    "maxAdults": 3,
    "price": 172,
    "goods": [
      "Washer",
      "Baby seat",
      "Air conditioning",
      "Laptop friendly workspace",
      "Towels",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 52.367540000000005,
      "longitude": 4.883976,
      "zoom": 16
    },
    "id": 74
  },
  {
    "city": {
      "name": "Paris",
      "location": {
        "latitude": 48.85661,
        "longitude": 2.351499,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/15.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.5,
    "type": "hotel",
    "bedrooms": 3,
    "maxAdults": 10,
    "price": 201,
    "goods": [
      "Baby seat",
      "Air conditioning",
      "Towels",
      "Laptop friendly workspace",
      "Breakfast",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "This is a place for dreamers to reset, reflect, and create. Designed with a 'slow' pace in mind, our hope is that you enjoy every part of your stay; from making local coffee by drip in the morning, choosing the perfect record to put on as the sun sets.",
    "location": {
      "latitude": 48.846610000000005,
      "longitude": 2.374499,
      "zoom": 16
    },
    "id": 75
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/14.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.5,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 2,
    "price": 282,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace",
      "Washer",
      "Air conditioning"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I rent out a very sunny and bright apartment only 7 minutes walking distance to the metro station. The apartment has a spacious living room with a kitchen, one bedroom and a bathroom with mit bath. A terrace can be used in summer.",
    "location": {
      "latitude": 50.950361,
      "longitude": 6.961974,
      "zoom": 16
    },
    "id": 76
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/12.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "Beautiful & luxurious apartment at great location",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.1,
    "type": "apartment",
    "bedrooms": 4,
    "maxAdults": 6,
    "price": 229,
    "goods": [
      "Fridge",
      "Coffee machine",
      "Dishwasher",
      "Air conditioning",
      "Baby seat",
      "Washer",
      "Breakfast",
      "Washing machine",
      "Towels",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 50.941361,
      "longitude": 6.956974,
      "zoom": 16
    },
    "id": 77
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/3.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg"
    ],
    "title": "Amazing and Extremely Central Flat",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.2,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 9,
    "price": 912,
    "goods": [
      "Laptop friendly workspace",
      "Baby seat",
      "Washer",
      "Air conditioning",
      "Towels",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Design interior in most sympathetic area! Complitely renovated, well-equipped, cosy studio in idyllic, over 100 years old wooden house. Calm street, fast connection to center and airport.",
    "location": {
      "latitude": 50.833557,
      "longitude": 4.374696999999999,
      "zoom": 16
    },
    "id": 78
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/18.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg"
    ],
    "title": "Perfectly located Castro",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.2,
    "type": "hotel",
    "bedrooms": 2,
    "maxAdults": 6,
    "price": 116,
    "goods": [
      "Washer",
      "Laptop friendly workspace",
      "Air conditioning",
      "Breakfast"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Discover daily local life in city center, friendly neighborhood, clandestine casino, karaoke, old-style artisans, art gallery and artist studio downstairs.",
    "location": {
      "latitude": 51.236402000000005,
      "longitude": 6.784314,
      "zoom": 16
    },
    "id": 79
  },
  {
    "city": {
      "name": "Dusseldorf",
      "location": {
        "latitude": 51.225402,
        "longitude": 6.776314,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/7.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.4,
    "type": "hotel",
    "bedrooms": 4,
    "maxAdults": 6,
    "price": 103,
    "goods": [
      "Baby seat",
      "Towels",
      "Coffee machine",
      "Fridge",
      "Air conditioning",
      "Washer",
      "Washing machine",
      "Dishwasher",
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 51.217402,
      "longitude": 6.7693140000000005,
      "zoom": 16
    },
    "id": 80
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/3.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 3.4,
    "type": "apartment",
    "bedrooms": 2,
    "maxAdults": 10,
    "price": 408,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 53.573341000000006,
      "longitude": 10.025654000000001,
      "zoom": 16
    },
    "id": 81
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/15.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg"
    ],
    "title": "Waterfront with extraordinary view",
    "isFavorite": false,
    "isPremium": true,
    "rating": 3.4,
    "type": "room",
    "bedrooms": 1,
    "maxAdults": 1,
    "price": 162,
    "goods": [
      "Breakfast",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 52.37554,
      "longitude": 4.9019759999999994,
      "zoom": 16
    },
    "id": 82
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/7.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg"
    ],
    "title": "Loft Studio in the Central Area",
    "isFavorite": false,
    "isPremium": true,
    "rating": 4.6,
    "type": "house",
    "bedrooms": 3,
    "maxAdults": 7,
    "price": 337,
    "goods": [
      "Towels",
      "Breakfast",
      "Laptop friendly workspace",
      "Dishwasher",
      "Fridge",
      "Air conditioning",
      "Baby seat",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 52.385540000000006,
      "longitude": 4.902976,
      "zoom": 16
    },
    "id": 83
  },
  {
    "city": {
      "name": "Cologne",
      "location": {
        "latitude": 50.938361,
        "longitude": 6.959974,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/17.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg"
    ],
    "title": "The house among olive ",
    "isFavorite": false,
    "isPremium": true,
    "rating": 2.8,
    "type": "house",
    "bedrooms": 3,
    "maxAdults": 5,
    "price": 482,
    "goods": [
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Relax, rejuvenate and unplug in this ultimate rustic getaway experience in the country. In our beautiful screened Pondhouse, you can gaze at the stars and listen to the sounds of nature from your cozy warm bed.",
    "location": {
      "latitude": 50.960361,
      "longitude": 6.9509739999999995,
      "zoom": 16
    },
    "id": 84
  },
  {
    "city": {
      "name": "Hamburg",
      "location": {
        "latitude": 53.550341,
        "longitude": 10.000654,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/14.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/18.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "Wood and stone place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.6,
    "type": "house",
    "bedrooms": 4,
    "maxAdults": 8,
    "price": 736,
    "goods": [
      "Laptop friendly workspace",
      "Towels",
      "Air conditioning",
      "Breakfast",
      "Washer",
      "Baby seat"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "Design interior in most sympathetic area! Complitely renovated, well-equipped, cosy studio in idyllic, over 100 years old wooden house. Calm street, fast connection to center and airport.",
    "location": {
      "latitude": 53.563341,
      "longitude": 10.019654000000001,
      "zoom": 16
    },
    "id": 85
  },
  {
    "city": {
      "name": "Amsterdam",
      "location": {
        "latitude": 52.37454,
        "longitude": 4.897976,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/1.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/2.jpg",
      "https://12.react.pages.academy/static/hotel/13.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/5.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/20.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/7.jpg",
      "https://12.react.pages.academy/static/hotel/1.jpg"
    ],
    "title": "The Pondhouse - A Magical Place",
    "isFavorite": false,
    "isPremium": false,
    "rating": 4.3,
    "type": "hotel",
    "bedrooms": 2,
    "maxAdults": 5,
    "price": 251,
    "goods": [
      "Breakfast",
      "Air conditioning",
      "Towels",
      "Washing machine",
      "Laptop friendly workspace",
      "Dishwasher",
      "Baby seat",
      "Fridge",
      "Coffee machine",
      "Washer"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "I am happy to welcome you to my apartment in the city center! Three words: location, cosy and chic!",
    "location": {
      "latitude": 52.36554,
      "longitude": 4.911976,
      "zoom": 16
    },
    "id": 86
  },
  {
    "city": {
      "name": "Brussels",
      "location": {
        "latitude": 50.846557,
        "longitude": 4.351697,
        "zoom": 13
      }
    },
    "previewImage": "https://12.react.pages.academy/static/hotel/16.jpg",
    "images": [
      "https://12.react.pages.academy/static/hotel/1.jpg",
      "https://12.react.pages.academy/static/hotel/15.jpg",
      "https://12.react.pages.academy/static/hotel/14.jpg",
      "https://12.react.pages.academy/static/hotel/16.jpg",
      "https://12.react.pages.academy/static/hotel/19.jpg",
      "https://12.react.pages.academy/static/hotel/12.jpg",
      "https://12.react.pages.academy/static/hotel/17.jpg",
      "https://12.react.pages.academy/static/hotel/6.jpg",
      "https://12.react.pages.academy/static/hotel/11.jpg",
      "https://12.react.pages.academy/static/hotel/4.jpg",
      "https://12.react.pages.academy/static/hotel/9.jpg",
      "https://12.react.pages.academy/static/hotel/10.jpg",
      "https://12.react.pages.academy/static/hotel/8.jpg",
      "https://12.react.pages.academy/static/hotel/3.jpg"
    ],
    "title": "Nice, cozy, warm big bed apartment",
    "isFavorite": false,
    "isPremium": false,
    "rating": 2.9,
    "type": "hotel",
    "bedrooms": 3,
    "maxAdults": 10,
    "price": 303,
    "goods": [
      "Coffee machine",
      "Towels",
      "Washing machine",
      "Washer",
      "Air conditioning",
      "Baby seat",
      "Fridge",
      "Breakfast",
      "Dishwasher",
      "Laptop friendly workspace"
    ],
    "host": {
      "id": 25,
      "name": "Angelina",
      "isPro": true,
      "avatarUrl": "img/avatar-angelina.jpg"
    },
    "description": "A new spacious villa, one floor. All commodities, jacuzzi and beautiful scenery. Ideal for families or friends.",
    "location": {
      "latitude": 50.828556999999996,
      "longitude": 4.362697,
      "zoom": 16
    },
    "id": 87
  }
];

export {
    rooms
};
