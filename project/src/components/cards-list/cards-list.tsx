import CitiesCard from "../cities-card/cities-card";
import { Room } from "../../types/room";

type CardsListProps = {
  rooms: Room[];
}

function CardsList({rooms}: CardsListProps): JSX.Element {
  const roomsList = rooms;

  return (
    <div className="cities__places-list places__list tabs__content">
      {
        roomsList.map((room)=> {
          return (
            <CitiesCard key={room.id} room={room}/>
          );
        })
      }
    </div>
  )
}


export default CardsList;
