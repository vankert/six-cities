import MainScreen from "../../pages/main-screen/main-screen";
import RoomScreen from "../../pages/room-screen/room-screen";
import LoginScreen from "../../pages/login-screen/login-screen";
import FavoritesScreen from "../../pages/favorites-screen/favorites-screen";
import NotFoundScreen from "../../pages/404-screen/404-screen";
import ScrollToTop from "../scroll-to-top/scroll-to-top";
import PrivateRoute from "../private-route/private-route";

import {HelmetProvider} from 'react-helmet-async';

import {BrowserRouter, Routes, Route} from 'react-router-dom';
import { AuthorizationStatus } from "../../const";

import { rooms } from "../../mock/rooms";

function App(): JSX.Element {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <ScrollToTop />
        <Routes>
          <Route path="/" element={<MainScreen rooms={rooms}/>} />
          <Route path="/login" element={<LoginScreen />} />
          <Route path="/offer/:id" element={<RoomScreen rooms={rooms} />} />
          <Route path="/favorite" element={
            <PrivateRoute
            authorizationStatus={AuthorizationStatus.Auth}
            >
              <FavoritesScreen rooms={rooms} />
            </PrivateRoute>
          } />
          <Route path="*" element={<NotFoundScreen />} />
        </Routes>
      </BrowserRouter>
    </HelmetProvider>
  );
}

export default App;
