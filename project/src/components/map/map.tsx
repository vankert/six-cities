import { Room } from "../../types/room";

import {useRef, useEffect} from 'react';
import {Icon, Marker} from 'leaflet';
import useMap from "../../hooks/useMap";
import 'leaflet/dist/leaflet.css';


type MapProps = {
  centerRoom: Room;
  points: Room[];
  selectedRoom: Room | undefined;
};

const defaultCustomIcon = new Icon({
  iconUrl: 'img/pin.svg',
  iconSize: [40, 40],
  iconAnchor: [20, 40]
});

const currentCustomIcon = new Icon({
  iconUrl: 'img/pin-active.svg',
  iconSize: [40, 40],
  iconAnchor: [20, 40]
});



function Map(props: MapProps): JSX.Element {
  const {centerRoom, points, selectedRoom} = props;

  const mapRef = useRef(null);
  const map = useMap(mapRef, centerRoom);

  useEffect(() => {
    if (map) {
      points.forEach((point) => {
        const marker = new Marker({
          lat: point.location.latitude,
          lng: point.location.longitude
        });

        marker
          .setIcon(
            selectedRoom !== undefined && point.title === selectedRoom.title
              ? currentCustomIcon
              : defaultCustomIcon
          )
          .addTo(map);
      });
    }
  }, [map, points, selectedRoom]);

  return (
    <section ref={mapRef} style={{height: '780px'}} className="cities__map map"></section>
  )
}

export default Map;
