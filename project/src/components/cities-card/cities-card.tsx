import { useState } from "react";
import { Room } from "../../types/room";
import {Link} from 'react-router-dom';
import {useLocation} from 'react-router-dom';

type CitiesCardProps = {
  room: Room;
}

function CitiesCard({room}: CitiesCardProps): JSX.Element {
  const {pathname} = useLocation();
  const [isFavorite, setIsFavorite] = useState(room.isFavorite);
  const [cardId, setCardId] = useState<number>();

  const hoverCardHandler = () => {
    if(pathname == '/') {
      setCardId(room.id);
    }
  }

  const ratingStyle = Math.floor(room.rating) * 20;

  const checkCardMainClass = (path: string): string => {
    if(path == '/favorite') {
      return 'favorites__card place-card';
    } else if(path == '/') {
      return 'cities__card place-card'
    } else {
      return 'near-places__card place-card';
    }
  };

  const checkCardImgClass = (path: string): string => {
    if(path == '/favorite') {
      return 'favorites__image-wrapper place-card__image-wrapper';
    } else if(path == '/') {
      return 'cities__image-wrapper place-card__image-wrapper'
    } else {
      return 'near-places__image-wrapper place-card__image-wrapper';
    }
  };

  const checkCardInfoClass = (path: string): string => {
    if(path == '/favorite') {
      return 'favorites__card-info place-card__info';
    } else {
      return 'place-card__info'
    }
  };

  return (
    <article onMouseMove={hoverCardHandler} className={checkCardMainClass(pathname)}>
      {room.isPremium ? <div className="place-card__mark"><span>Premium</span></div> : null}

      <div className={checkCardImgClass(pathname)}>
        <Link to={`/offer/${room.id}`}>
          <img className="place-card__image" src={room.previewImage} width="260" height="200" alt="Place image"/>
        </Link>
      </div>
      <div className={checkCardInfoClass(pathname)}>
        <div className="place-card__price-wrapper">
          <div className="place-card__price">
            <b className="place-card__price-value">&euro;{room.price}</b>
            <span className="place-card__price-text">&#47;&nbsp;night</span>
          </div>
          <button
            onClick={()=>setIsFavorite(!isFavorite)}
            className={`place-card__bookmark-button button ${isFavorite ? 'place-card__bookmark-button--active' : ''}`}
            type="button"
          >
            <svg className="place-card__bookmark-icon" width="18" height="19">
              <use xlinkHref="#icon-bookmark"></use>
            </svg>
            <span className="visually-hidden">To bookmarks</span>
          </button>
        </div>
        <div className="place-card__rating rating">
          <div className="place-card__stars rating__stars">
            <span style={{ width: `${String(ratingStyle)}%` }}></span>
            <span className="visually-hidden">Rating</span>
          </div>
        </div>
        <h2 className="place-card__name">
          <Link to={`/offer/${room.id}`}>{room.title}</Link>
        </h2>
        <p className="place-card__type">{room.type}</p>
      </div>
    </article>
  );
}

export default CitiesCard;
