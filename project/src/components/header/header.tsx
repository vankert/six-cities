import {Link} from 'react-router-dom';

function Header(): JSX.Element {
  const loginStatus = true;

  return (
    <header className="header">
        <div className="container">
          <div className="header__wrapper">
            <div className="header__left">
              <Link to="/" className="header__logo-link">
                <img className="header__logo" src="img/logo.svg" alt="6 cities logo" width="81" height="41"/>
              </Link>
            </div>
            <nav className="header__nav">
              <ul className="header__nav-list">
                {loginStatus ? (
                  <li className="header__nav-item user">
                    <Link to="/favorite" className="header__nav-link header__nav-link--profile" >
                      <div className="header__avatar-wrapper user__avatar-wrapper">
                      </div>
                      <span className="header__user-name user__name">Oliver.conner@gmail.com</span>
                      <span className="header__favorite-count">3</span>
                    </Link>
                  </li>
                ) : null}

                <li className="header__nav-item">
                  {loginStatus ? (
                    <a className="header__nav-link" href="#">
                      <span className="header__signout">Sign out</span>
                    </a>
                  ) : (
                    <a className="header__nav-link" href="#">
                      <span className="header__signin">Sign in</span>
                    </a>
                  )}

                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
  );
}

export default Header;
